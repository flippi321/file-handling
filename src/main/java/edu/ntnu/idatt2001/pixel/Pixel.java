package edu.ntnu.idatt2001.pixel;

/**
 * A class for a pixel.
 * A pixel represents a location in (x,y) coordinate space, specified in integer precision, and has a color.
 */
public class Pixel {

    private final int x;
    private final int y;
    private final Color color;

    public Pixel(int x, int y, Color color) {
        if (color == null) {
            throw new IllegalArgumentException("A color cannot be null");
        }
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "Pixel{" +
                "x=" + x +
                ", y=" + y +
                ", color=" + color +
                '}';
    }
}
