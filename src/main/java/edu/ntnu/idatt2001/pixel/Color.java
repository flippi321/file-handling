package edu.ntnu.idatt2001.pixel;

/**
 * A class representing a color in RGB format.
 */
public class Color {

    private final int red;
    private final int green;
    private final int blue;

    /**
     * Constructor for a color in hex code format.
     * @param hexCodeRGB The color in hex code RGB format, e.g. FF0033.
     */
    public Color(String hexCodeRGB) {
        if (!isValidHexCodeRGB(hexCodeRGB)) {
            throw new IllegalArgumentException("'" + hexCodeRGB + "' is not a valid hexadecimal color code");
        }

        this.red = Integer.parseInt(hexCodeRGB.substring(0, 2), 16);
        this.green = Integer.parseInt(hexCodeRGB.substring(2, 4), 16);
        this.blue = Integer.parseInt(hexCodeRGB.substring(4, 6), 16);
    }


    /**
     * Constructor for a color in decimal format.
     * @param red Intensity of red (0 - 255).
     * @param green Intensity of green (0 - 255).
     * @param blue Intensity of blue (0 - 255).
     */
    public Color(int red, int green, int blue) {
        if (!isValidDecimalCode(red)) {
            throw new IllegalArgumentException("'" + red + "' is not a valid decimal color code for red");
        }
        if (!isValidDecimalCode(green)) {
            throw new IllegalArgumentException("'" + green + "' is not a valid decimal color code for green");
        }
        if (!isValidDecimalCode(blue)) {
            throw new IllegalArgumentException("'" + blue + "' is not a valid decimal color code for blue");
        }

        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public int getRed() {
        return red;
    }

    public int getGreen() {
        return green;
    }

    public int getBlue() {
        return blue;
    }

    public String getHexCodeRGB() {
        String r = red < 16 ? "0" + Integer.toHexString(red) : Integer.toHexString(red);
        String g = green < 16 ? "0" + Integer.toHexString(green) : Integer.toHexString(green);
        String b = blue < 16 ? "0" + Integer.toHexString(blue) : Integer.toHexString(blue);
        return r+g+b;
    }

    @Override
    public String toString() {
        return "Color{" +
                "red=" + red +
                ", green=" + green +
                ", blue=" + blue +
                '}';
    }

    // A valid hex code has 6 characters (A-F, a-f, 0-9).
    // Each color is represented by two characters.
    private boolean isValidHexCodeRGB(String hexCodeRGB) {
        final String regex = "[A-Fa-f0-9]{6}";
        return hexCodeRGB.matches(regex);
    }

    // A valid decimal code is an integer >=0 and <= 255.
    private boolean isValidDecimalCode(int decimalCode) {
        return (decimalCode >= 0 && decimalCode <= 255);
    }

}
