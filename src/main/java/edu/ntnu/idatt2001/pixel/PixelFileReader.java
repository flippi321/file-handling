package edu.ntnu.idatt2001.pixel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * A PixelFileReader reads pixel data from a text file.
 * Each line in the file represents a pixel and must follow the format 'x y color',
 * where x and y are integers and color is a hex color code.
 * <p>
 * Sample file data:
 *
 * <pre>{@code}
 * 1 1 ff0000
 * 1 2 00ff00
 * 1 3 0000ff
 * 10 280 cccccc
 * {@code}</pre>
 * </p>
 */
public class PixelFileReader {

    private static final String DELIMITER = " ";

    public PixelFileReader() {}

    /**
     * Read a list of pixels from a text file.
     * @param file An abstract representation of a file containing pixel data.
     * @return A list of {@link Pixel} objects.
     * @throws IOException If an I/O error occurs.
     */
    public List<Pixel> readPixels(File file) throws IOException {
        if (!file.getName().endsWith(".txt")) {
            throw new IOException("Unsupported file format. Only .txt-files are supported.");
        }

        List<Pixel> pixels = new ArrayList<>();
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                String[] tokens = line.split(DELIMITER);
                if (tokens.length != 3) {
                    throw new IOException("Line data '" + line + "' is invalid. Make sure each line is on the form 'x y color'");
                }

                int x;
                int y;
                try {
                    x = Integer.parseInt(tokens[0]);
                    y = Integer.parseInt(tokens[1]);
                } catch (NumberFormatException e) {
                    throw new IOException("X and Y must be integers (" + e.getMessage() + ")");
                }
                String hexColorRGB = tokens[2];

                Color color = new Color(hexColorRGB);
                Pixel pixel = new Pixel(x, y, color);
                pixels.add(pixel);
            }
        } catch (IOException e) {
            throw new IOException("Unable to read pixel data from file '" + file.getName() + "': " + e.getMessage());
        }
        return pixels;
    }
}
