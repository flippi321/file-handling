package edu.ntnu.idatt2001.pixel;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * A PixelFileWriter makes it possible to write {@link Pixel} objects to a file.
 * Each pixel is persisted in the format 'x y color'.
 * <p>
 * Sample file data:
 *
 * <pre>{@code}
 * 1 1 ff0000
 * 1 2 00ff00
 * 1 3 0000ff
 * 10 280 cccccc
 * {@code}</pre>
 * </p>
 */
public class PixelFileWriter {

    private static final String DELIMITER = " ";
    private static final String NEWLINE = "\n";

    PixelFileWriter() {}

    /**
     * Write a list of {@link Pixel} objects to a file.
     * @param pixels A list of {@link Pixel} objects.
     * @param file  An abstract representation of a file.
     * @throws IOException If an I/O error occurs.
     */
    public void writePixels(List<Pixel> pixels, File file) throws IOException {
        if (!file.getName().endsWith(".txt")) {
            throw new IOException("Unsupported file format. Only .txt-files are supported.");
        }

        try (FileWriter fileWriter = new FileWriter(file)) {
            for (Pixel pixel : pixels) {
                String line = pixel.getX() + DELIMITER + pixel.getY() + DELIMITER + pixel.getColor().getHexCodeRGB();
                fileWriter.write(line + NEWLINE);
            }
        } catch (IOException e) {
            throw new IOException("Unable to write pixel file: " + e.getMessage());
        }
    }
}
