package edu.ntnu.idatt2001;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class BinaryFiles {

    /*
     * Copy a binary file.
     * The data of inputFile is written to outPutFile.
     */
    public static void copy(File inputFile, File outputFile) {
        FileInputStream in = null;
        FileOutputStream out = null;
        try {
            in = new FileInputStream(inputFile);
            out = new FileOutputStream(outputFile);

            int byteRead;
            while ((byteRead = in.read()) != -1) {
                out.write(byteRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /*
     * Copy a binary file using Java NIO.
     */
    public static void copyNIO(Path inputFilePath, Path outputFilePath) {
        try {
            byte[] bytes = Files.readAllBytes(inputFilePath);
            Files.write(outputFilePath, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
