package edu.ntnu.idatt2001.serialization;

import java.io.*;

public class Serializer {

    /*
     * Serialize an object to a file.
     * The object must implement java.io.Serializable.
     */
    public static void writeObjectToFile(Object object, File file) throws IOException  {
        try (FileOutputStream fs = new FileOutputStream(file);
             ObjectOutputStream os = new ObjectOutputStream(fs)) {
            os.writeObject(object);
        }
    }
}
