package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Test;
import java.io.File;

/**
 * Class is used as a demo runner.
 */
class TextFilesTest {

    @Test
    void testWriteToFile() {
        TextFiles.writeToFile(new File("src/test/resources/file.txt"));
    }

    @Test
    void testWriteToFileTryWithResource() {
        TextFiles.writeToFileTryWithResource(new File("src/test/resources/file.txt"));
    }

    @Test
    void testWriteToFileStreamChaining() {
        TextFiles.writeToFileStreamChaining(new File("src/test/resources/file.txt"));
    }

    @Test
    void testReadFromFile() {
        TextFiles.readFromFile(new File("src/test/resources/file.txt"));
    }

    @Test
    void testReadLinesFromFile() {
       TextFiles.readLinesFromFile(new File("src/test/resources/file.txt"));
    }

    @Test
    void testWriteToFileNIO() {
        TextFiles.writeToFileNIO("src/test/resources/file.txt");
    }

    @Test
    void testReadLinesFromFileNIO() {
        TextFiles.readLinesFromFileNIO(new File("src/test/resources/file.txt"));
    }

    @Test
    void testReadLinesFromFileWithScanner() {
        TextFiles.readLinesFromFileWithScanner(new File("src/test/resources/file.txt"));
    }
}