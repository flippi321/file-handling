package edu.ntnu.idatt2001.serialization;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;

/**
 * Class is used as a demo runner.
 */
class DeserializerTest {

    @Test
    void testReadObjectFromFile() {
        File file = new File("src/test/resources/book.dat");
        Book book = new Book("978-1-292-15904-1", "Objects First with Java");

        try {
            Serializer.writeObjectToFile(book, file);
            Book bookFromFile = (Book)Deserializer.readObjectFromFile(file);
            assertEquals(book.toString(), bookFromFile.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}