package edu.ntnu.idatt2001.serialization;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

/**
 * Class is used as a demo runner.
 */
class SerializerTest {

    @Test
    void testWriteObjectToFile() {
        Book book = new Book("978-1-292-15904-1", "Objects First with Java");
        try {
            Serializer.writeObjectToFile(book, new File("src/test/resources/book.dat"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}