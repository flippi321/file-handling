package edu.ntnu.idatt2001.pixel;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class is used as a demo runner.
 */
class PixelFileWriterTest {

    @Test
    void testWritePixels() {
        PixelFileWriter pixelFileWriter = new PixelFileWriter();
        try {
            List<Pixel> pixels = new ArrayList<>();
            pixels.add(new Pixel(1, 1, new Color(255, 10, 0)));
            pixels.add(new Pixel(1, 2, new Color(0, 255, 8)));
            pixels.add(new Pixel(2060, 8, new Color(75, 0, 255)));

            pixelFileWriter.writePixels(pixels, new File("src/test/resources/pixels.txt"));
        } catch (IOException e) {
            fail();
        }
    }
}