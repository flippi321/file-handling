package edu.ntnu.idatt2001.pixel;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class is used as a demo runner.
 */
class PixelFileReaderTest {

    @Test
    void testReadPixelsFromFile() {
        PixelFileReader pixelFileReader = new PixelFileReader();
        try {
            List<Pixel> pixels = pixelFileReader.readPixels(new File("src/test/resources/pixels.txt"));
        } catch (IOException e) {
            fail();
        }
    }
}