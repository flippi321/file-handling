package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Class is used as a demo runner.
 */
public class BinaryFilesTest {

    @Test
    void testCopy() {
        File inputFile = new File("src/test/resources/input.png");
        File outputFile = new File("src/test/resources/output.png");
        BinaryFiles.copy(inputFile, outputFile);
    }

    @Test
    void testCopyNIO() {
        Path inputFilePath = Paths.get("src/test/resources/input.png");
        Path outputFilePath = Paths.get("src/test/resources/output.png");
        BinaryFiles.copyNIO(inputFilePath, outputFilePath);
    }
}
